##Raster processing=group
##Raster = raster
##grid_side= number 10
library(raster)
library(rgdal)
library(RODBC)
r<-Raster
r_nm<-r@file@name

command <- paste("raster2pgsql -d  -M -R ",r_nm, " -F -t ",grid_side,"x",grid_side," tmp|psql -d lidar",sep="")
system(command)

con<-odbcConnect("lidar")

query<-"CREATE OR REPLACE FUNCTION median (float[]) RETURNS float AS '
x<-arg1
x<-as.numeric(as.character(x))
x<-na.omit(x)
median(x)'
LANGUAGE 'plr' STRICT;
CREATE OR REPLACE FUNCTION q10 (float[]) RETURNS float AS '
x<-arg1
x<-as.numeric(as.character(x))
x<-na.omit(x)
quantile(x,0.1,na.rm=TRUE)'
LANGUAGE 'plr' STRICT;
CREATE OR REPLACE FUNCTION q90 (float[]) RETURNS float AS '
x<-arg1
x<-as.numeric(as.character(x))
x<-na.omit(x)
quantile(x,0.9,na.rm=TRUE)'
LANGUAGE 'plr' STRICT;"

odbcQuery(con,query)

query<-"drop table if exists tmp2;
create table tmp2 as
select rid, st_envelope(rast) geom,
q10((st_dumpvalues(rast)).valarray) q10,
median((st_dumpvalues(rast)).valarray) median,
q90((st_dumpvalues(rast)).valarray) q90
from tmp"

odbcQuery(con,query)

